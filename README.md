
# Emberstone

Spawn optimus in a breeze


## Installation

### Requirements

- Docker

### Setup

Clone this repository

```bash
git clone
```

Copy `.env` and `docker-compose.yml` files

```bash
  cp .env.exemple .env
  cp docker-compose.yml.example docker-compose.yml
```

From the `.env` file, define your URLs and set the path to the existing optimus repositories

### Launch

Start the services

```bash
docker-compose up
```

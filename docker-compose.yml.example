version: '3'
services:

  nginx:
    container_name: ${APP_PROJECT}-nginx
    image: nginx:stable-alpine
    env_file:
      - .env
    volumes:
      - ./config/nginx/nginx.conf:/etc/nginx/templates/default.conf.template
      - ./logs/nginx:/var/log/nginx
      - ${API_PATH}:/srv/api
      - ${WEB_PATH}:/srv/www
    # ports:
    #   - "80:80"
    labels:
      - "traefik.http.routers.${APP_PROJECT}.rule=Host(`${WEB_URL}`)"
      - "traefik.http.services.${APP_PROJECT}-service.loadbalancer.server.port=80"
      - "traefik.http.routers.${APP_PROJECT}-api.rule=Host(`${API_URL}`)"
      - "traefik.http.services.${APP_PROJECT}-api-service.loadbalancer.server.port=80"
    depends_on:
      - php-fpm

  php-fpm:
    container_name: ${APP_PROJECT}-php-fpm
    image: ${APP_PROJECT}-php-fpm
    build:
      context: .
      dockerfile: config/php/Dockerfile
    env_file:
      - .env
    volumes:
      - ${API_PATH}:/srv/api
      - ${WEB_PATH}:/srv/www
    user: "${UID:-1000}:${GID:-1000}"

# docker-compose run --rm composer require identifier/package
  composer:
    image: ${APP_PROJECT}-php-fpm
    container_name: ${APP_PROJECT}-composer
    env_file:
      - .env
    volumes:
      - ${API_PATH}:/srv/api
      - ${WEB_PATH}:/srv/www
    user: "${UID:-1000}:${GID:-1000}"
    working_dir: /srv/www
    command: -V
    entrypoint: ['/usr/bin/composer']

# docker-compose run --rm npm install
  npm:
    image: node:alpine
    container_name: ${APP_PROJECT}-npm
    env_file:
      - .env
    volumes:
      - ${API_PATH}:/srv/api
      - ${WEB_PATH}:/srv/www
    user: "${UID:-1000}:${GID:-1000}"
    working_dir: /srv/www
    user: "node"
    command: -v
    entrypoint: ['npm']

  mariadb:
    container_name: ${APP_PROJECT}-mariadb
    image: mariadb:latest
    # ports:
    #   - "${FORWARD_DB_PORT:-3306}:${DB_PORT:-3306}"
    env_file:
      - .env
    environment:
      - MYSQL_ROOT_PASSWORD=${DB_PASSWORD}
      - MYSQL_DATABASE=${DB_DATABASE}
      - MYSQL_USER=${DB_USERNAME}
      - MYSQL_PASSWORD=${DB_PASSWORD}
      - TZ=Europe/Paris
    volumes:
      - ./database:/var/lib/mysql
      - ${ALLSPARK_PATH}/mariadb/2020-08-01.sql:/docker-entrypoint-initdb.d/setup.sql

  # redis:
  #   container_name: ${APP_PROJECT}-redis
  #   image: 'redis:alpine'
  #  env_file:
  #    - .env
  #   ports:
  #     - '${FORWARD_REDIS_PORT:-6379}:6379'
  #   volumes:
  #     - ./cache:/data

  # memcached:
  #   container_name: ${APP_PROJECT}-memcached
  #   image: 'memcached:alpine'
  #  env_file:
  #    - .env
  #   ports:
  #     - '${FORWARD_MEMCACHED_PORT:-11211}:11211'

networks:
  default:
    external:
      name: web
